build:
	poetry build

build_and_publish_to_pypi:
	poetry publish --build

.PHONY: build build_and_publish_to_pypi
